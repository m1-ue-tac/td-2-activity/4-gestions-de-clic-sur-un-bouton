package com.univlille.clicsurunbouton;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Dans cette seconde solution, on utilise un gestionnaire de clic centralisé. Ceci est réalisé
 * par une méthode qui récupère des clics et qui cherche à savoir qui les a envoyés.
 */
public class Solution2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solution2);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // on récupère les boutons et on leur associe tous le même gestionnaire de clic
        findViewById(R.id.btn1_solution4).setOnClickListener(gestionnaireCentralise);
        findViewById(R.id.btn2_solution4).setOnClickListener(gestionnaireCentralise);
        findViewById(R.id.btn_back_solution4).setOnClickListener(gestionnaireCentralise);
    }

    // Cette méthode va regarder d'où vient le clic et choisir l'action à réaliser en conséquence
    View.OnClickListener gestionnaireCentralise = new View.OnClickListener() {
        public void onClick(View view) {
            // le paramètre view est l'objet qui a produit un clic géré par cette méthode (grâce à
            // la définition des setOnClickListener plus haut)

            // on récupère l'id de cet objet cliqué et on regarde lequel est-ce pour savoir quelle
            // action on veut déclencher.
            switch (view.getId()) {

                // si c'est le bouton 1
                case R.id.btn1_solution4:
                    Toast.makeText(view.getContext(), "Bouton 1 cliqué", Toast.LENGTH_SHORT).show();
                    break;
                // si c'est le bouton 2
                case R.id.btn2_solution4:
                    Toast.makeText(view.getContext(), "Bouton 2 cliqué", Toast.LENGTH_SHORT).show();
                    break;
                // si c'est le bouton 3
                case R.id.btn_back_solution4:
                    Toast.makeText(view.getContext(), "Bouton 3 cliqué", Toast.LENGTH_SHORT).show();
                    finish();
                    break;
            }
        }
    };
}