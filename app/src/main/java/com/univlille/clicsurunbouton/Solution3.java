package com.univlille.clicsurunbouton;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Dans cette troisième solution, on déclare le listener (cad le gestionnaire du clic) directement
 * avec le bouton.
 * C'est la méthode la plus couramment employée, même si pas la plus lisible...
 */

public class Solution3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solution3);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // ici, on récupère le bouton dans une variable, et on lui associe ensuite
        // son listener/écouteur.
        // Cette façon de faire permet ensuite de manipuler le bouton grâce à la variable bouton1
        // par exemple en changeant sa couleur, son texte, etc.
        // Exemple :
        // bouton1.setText("Nouveau texte du bouton");
        Button bouton1 = (Button) findViewById(R.id.btn1_solution4);
        bouton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Bouton 1 cliqué", Toast.LENGTH_SHORT).show();
            }
        });

        // même chose ici
        Button bouton2 = (Button) findViewById(R.id.btn2_solution4);
        bouton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Bouton 2 cliqué", Toast.LENGTH_SHORT).show();
            }
        });

        // ici, on a écrit un code + simple car si on n'a pas besoin d'une variable pour gérer le bouton
        // on peut se passer de déclarer cette variable. On associe donc simplement le listener
        // au bouton. Par contre, on ne peut pas modifier le texte du bouton par la suite car
        // pas de variable associée au bouton.
        findViewById(R.id.btn_back_solution4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Bouton back cliqué", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}