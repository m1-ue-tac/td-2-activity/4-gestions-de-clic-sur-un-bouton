package com.univlille.clicsurunbouton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Dans cette quatrième solution, on déclare le bouton cliquable dans le XML du layout.
 * Pour cela, on donne le nom de la méthode qui sera appelée (on ne donne que le nom !).
 * cf. dans le layout :  android:onClick="solution4"
 * La méthode est déclarée dans l'activité associée au layout, donc ici pour cet exemple.
 */

public class Solution4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solution4);
    }

    // Méthode déclarée dans le XML du layout.
    // Elle doit être déclarée avec un paramètre de type View car c'est toujours une View qui
    // envoie des clics (des événements de façon générale)
    public void solution4(View view) {
        // on récupère l'id de l'obet cliqué et on exécute l'action associée à cet objet.
        switch (view.getId()) {
            // si c'est le bouton 1
            case R.id.btn1_solution4:
                Toast.makeText(view.getContext(), "Bouton 1 cliqué", Toast.LENGTH_SHORT).show();
                break;
            // si c'est le bouton 2
            case R.id.btn2_solution4:
                Toast.makeText(view.getContext(), "Bouton 2 cliqué", Toast.LENGTH_SHORT).show();
                break;
            // si c'est le bouton 3
            case R.id.btn_back_solution4:
                Toast.makeText(view.getContext(), "Bouton back cliqué", Toast.LENGTH_SHORT).show();
                finish();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }
}