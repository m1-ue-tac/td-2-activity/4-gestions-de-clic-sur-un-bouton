package com.univlille.clicsurunbouton;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Cet exemple montre 4 solutions différentes pour gérer le clic sur un bouton.
 * Vous trouverez 4 activités avec le même layout, mais utilisant chaque fois
 * une solution différente pour gérer le clic des boutons.
 * Pour information, la solution 4 utilise la même solution qu'ici.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Gestion du clic comme dans la solution 4 (cf. l'activité Solution4)
    public void solution(View view) {
        switch (view.getId()) {
            case R.id.solution1:
                Intent solution1 = new Intent(view.getContext(), Solution1.class);
                startActivity(solution1);
                break;
            case R.id.solution2:
                Intent solution2 = new Intent(view.getContext(), Solution2.class);
                startActivity(solution2);
                break;
            case R.id.solution3:
                Intent solution3 = new Intent(view.getContext(), Solution3.class);
                startActivity(solution3);
                break;
            case R.id.solution4:
                Intent solution4 = new Intent(view.getContext(), Solution4.class);
                startActivity(solution4);
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }
}