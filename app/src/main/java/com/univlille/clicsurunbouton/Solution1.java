package com.univlille.clicsurunbouton;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

/**
 * Dans cette première solution, on déclare les écouteurs dans des méthodes séparées.
 */
public class Solution1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solution1);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // on récupère les boutons et on leur associe leur gestionnaire de clic
        findViewById(R.id.btn1_solution4).setOnClickListener(gestionClicBouton1);
        findViewById(R.id.btn2_solution4).setOnClickListener(gestionClicBouton2);
        findViewById(R.id.btn_back_solution4).setOnClickListener(gestionClicBoutonBack);
    }

    // méthode pour gérer le clic du bouton 1
    View.OnClickListener gestionClicBouton1 = new View.OnClickListener() {
        public void onClick(View view) {
            // ici on affiche un simple Toast
            Toast.makeText(view.getContext(), "Bouton 1 cliqué", Toast.LENGTH_SHORT).show();
            // Snackbar.make(view, "Bouton 1 cliqué", BaseTransientBottomBar.LENGTH_LONG).show();
        }
    };

    View.OnClickListener gestionClicBouton2 = new View.OnClickListener() {
        public void onClick(View view) {
            // ici on a choisi d'afficher un SnackBar (pour faire + Material Design :-) )
            // Pour cela, il faut ajouter la dépendance :
            // implementation 'com.google.android.material:material:1.2.1'
            // dans le build.gradle (déjà fait pour ce projet)
            // Toast.makeText(view.getContext(), "Bouton 2 cliqué", Toast.LENGTH_SHORT).show();
            Snackbar.make(view, "Bouton 2 cliqué", BaseTransientBottomBar.LENGTH_LONG).show();
        }
    };

    View.OnClickListener gestionClicBoutonBack = new View.OnClickListener() {
        public void onClick(View view) {
            Toast.makeText(view.getContext(), "Bouton back cliqué", Toast.LENGTH_SHORT).show();
            // on ferme l'activité et donc on revient à la MainActivity
            finish();
        }
    };

}